WHERE'S THE CODE?
-----------------

You've just checked out the code from Git's "master" branch. In Drupal contrib
module development, the code lives in branches that match a naming convention
which mirrors the released version numbers.

For example:
The code for a project's 7.x-1.x-dev version is found in the 7.x-1.x Git branch.

To see a list of all branches for this project using Git on the command line,
use the following command:

  git branch -a

To switch from master branch to the 7.x-1.x branch using Git on the command
line, use the following command:

  git checkout 7.x-1.x


SUPPORT THE AUTHOR
------------------

This module was originally developed as an example module for Chapter 4 of the
book, Drupal 7 Module Development, by Matt Butcher, Greg Dunlap, Matt Farina,
Larry Garfield, Ken Rickard and John Albin Wilkins.

You can support the author by buying a copy of the book at:
http://www.amazon.com/dp/1849511160?tag=johnalbin-20
